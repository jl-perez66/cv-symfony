<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Identity;
use App\Entity\Skills;
use App\Entity\Formation;
use App\Entity\Realisation;
use Faker;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $this->loadIdentity($manager);
        $this->loadSkills($manager);
        $this->loadFormation($manager);
        $this->loadRealisation($manager);
        // $product = new Product();
        // $manager->persist($product);

    }

    public function loadIdentity($manager)
    {
        $identity = new Identity();
        $identity -> setFirstName('Jean-Luc');
        $identity -> setLastName('Perez');
        $identity -> setAge(19);
        $identity -> setTel('06 66 70 56 13');
        $identity -> setEmail('jeanlucperez@outlook.fr');
        $identity -> setSite('devjl.fr');
        $identity -> setPdf('cv.pdf');
        $identity -> setGit('https://gitlab.com/jl-perez66');
        $identity -> setLinkedin('https://www.linkedin.com/in/jean-luc-perez-871466165/');
        $this-> setReference('jl', $identity);
        $manager-> persist($identity);
        
        $manager->flush();
    }

    public function loadSkills($manager)
    {
        $skills = ['HTML','CSS','JavaScript','PHP','TypeScript','MySQL','Bootstrap','Symfony','Angular','npm','React','git','Twig'];
        foreach ($skills as $key => $value) { 
            $skill = new Skills;
            $skill -> setLabel($value);
            $skill -> addIdentity($this->getReference('jl'));
            $manager-> persist($skill);
        }
        $manager->flush();
    }

    public function loadFormation($manager)
    {
        $formation_title = ['Baccalauréat STI2D', 'BNJSP', 'PSE2', 'Diplôme de Développement Web (BAC + 2)'];

        foreach ($formation_title as $key => $value) {
            $formation = new Formation;
            $formation -> setTitle($value);
            $formation -> setDescription('Un diplôme parmis tant d\'autres.');
            $formation -> addIdentity($this->getReference('jl'));
            $manager->persist($formation);
            
            //Obsolète ?
            // $formation -> setDateStart($faker->date($format = 'Y-m-d', $max = 'now'));
            // $formation ->setDateEnd($faker->date($format = 'Y-m-d', $min = $formation));

        }
        $manager->flush();
    }

    public function loadRealisation($manager) {

        $devjl = new Realisation;
        $devjl -> setIdentity($this->getReference('jl'));
        $devjl -> setTitle('devjl.fr');
        $devjl -> setFinished(false);
        $manager -> persist($devjl);

        $realisations = ['PHP - Site type Toys\'R\'Us','WordPress - Site de modélisme', 'Symfony - Site de réservation de camping', 'MapBoxGL - Carte interactive d\'évènements'];

        foreach($realisations as $key => $value) {
            $realisation = new Realisation;
            $realisation -> setIdentity($this->getReference('jl'));
            $realisation -> setTitle($value);
            $realisation -> setFinished(true);
            $manager -> persist($realisation);
        }

        $manager->flush();

    }
}
