<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ErrorCVController extends AbstractController
{
    /**
     * @Route("/error/c/v", name="error_c_v")
     */
    public function index()
    {
        return $this->render('error_cv/index.html.twig', [
            'controller_name' => 'ErrorCVController',
        ]);
    }
}
