<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Identity;
use App\Entity\Skills;
use App\Entity\Formation;
use App\Repository\IdentityRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;

class CvController extends AbstractController
{
    /**
     * @Route("/cv/{id}", name="cv")
     */
    public function index(?Identity $id, IdentityRepository $repo)
    {
        if($id == null){
            return $this->redirectToRoute('error_c_v');
        }
        // dd($id);
        return $this->render('cv/index.html.twig', [
            'controller_name' => 'CvController',
            'user' => $id,
            'skills' => $id->getSkills(),
            'formations' => $id->getFormations(),
            'realisations' => $id->getRealisations()
        ]);
    }
}
