<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Repository\IdentityRepository;

class HomeController extends AbstractController
{
     


    //redirection vers home
    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        return $this->redirect('home');
    }
    /**
     * @Route("/home", name="home", methods={"GET"})
     */
    public function main(IdentityRepository $identRepo)
    {  
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
             'users' => $identRepo->findAll()
            
        ]);
    }
}
