<?php

namespace App\Repository;

use App\Entity\IdentitySkills;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method IdentitySkills|null find($id, $lockMode = null, $lockVersion = null)
 * @method IdentitySkills|null findOneBy(array $criteria, array $orderBy = null)
 * @method IdentitySkills[]    findAll()
 * @method IdentitySkills[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IdentitySkillsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IdentitySkills::class);
    }

    // /**
    //  * @return IdentitySkills[] Returns an array of IdentitySkills objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?IdentitySkills
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
