<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200305103706 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE formation (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE formation_identity (formation_id INT NOT NULL, identity_id INT NOT NULL, INDEX IDX_AD07BE355200282E (formation_id), INDEX IDX_AD07BE35FF3ED4A8 (identity_id), PRIMARY KEY(formation_id, identity_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE identity (id INT AUTO_INCREMENT NOT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, age INT NOT NULL, tel VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, site VARCHAR(255) DEFAULT NULL, pdf VARCHAR(255) DEFAULT NULL, git VARCHAR(255) DEFAULT NULL, linkedin VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE identity_skills (identity_id INT NOT NULL, skills_id INT NOT NULL, INDEX IDX_6866BF26FF3ED4A8 (identity_id), INDEX IDX_6866BF267FF61858 (skills_id), PRIMARY KEY(identity_id, skills_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE realisation (id INT AUTO_INCREMENT NOT NULL, identity_id INT NOT NULL, title VARCHAR(255) NOT NULL, finished TINYINT(1) NOT NULL, INDEX IDX_EAA5610EFF3ED4A8 (identity_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE skills (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE formation_identity ADD CONSTRAINT FK_AD07BE355200282E FOREIGN KEY (formation_id) REFERENCES formation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE formation_identity ADD CONSTRAINT FK_AD07BE35FF3ED4A8 FOREIGN KEY (identity_id) REFERENCES identity (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE identity_skills ADD CONSTRAINT FK_6866BF26FF3ED4A8 FOREIGN KEY (identity_id) REFERENCES identity (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE identity_skills ADD CONSTRAINT FK_6866BF267FF61858 FOREIGN KEY (skills_id) REFERENCES skills (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE realisation ADD CONSTRAINT FK_EAA5610EFF3ED4A8 FOREIGN KEY (identity_id) REFERENCES identity (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE formation_identity DROP FOREIGN KEY FK_AD07BE355200282E');
        $this->addSql('ALTER TABLE formation_identity DROP FOREIGN KEY FK_AD07BE35FF3ED4A8');
        $this->addSql('ALTER TABLE identity_skills DROP FOREIGN KEY FK_6866BF26FF3ED4A8');
        $this->addSql('ALTER TABLE realisation DROP FOREIGN KEY FK_EAA5610EFF3ED4A8');
        $this->addSql('ALTER TABLE identity_skills DROP FOREIGN KEY FK_6866BF267FF61858');
        $this->addSql('DROP TABLE formation');
        $this->addSql('DROP TABLE formation_identity');
        $this->addSql('DROP TABLE identity');
        $this->addSql('DROP TABLE identity_skills');
        $this->addSql('DROP TABLE realisation');
        $this->addSql('DROP TABLE skills');
    }
}
