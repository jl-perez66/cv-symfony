<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FormationRepository")
 */
class Formation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

   

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\identity", inversedBy="formations")
     */
    private $identity;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    public function __construct()
    {
        $this->identity = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Collection|identity[]
     */
    public function getIdentity(): Collection
    {
        return $this->identity;
    }

    public function addIdentity(identity $identity): self
    {
        if (!$this->identity->contains($identity)) {
            $this->identity[] = $identity;
        }

        return $this;
    }

    public function removeIdentity(identity $identity): self
    {
        if ($this->identity->contains($identity)) {
            $this->identity->removeElement($identity);
        }

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

}
