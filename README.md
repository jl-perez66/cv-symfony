# CV Symfony

Ce **CV** a été conçu avec le framework *Symfony*

Afin de le télécharger et l'utiliser, voici la conduite à tenir : 

[1] - **Requis**

- **PHP**
- **Composer**
- **Symfony** *(v4.12)*
-----
[2] - **Installation**

Tout d'abord, écrire les commandes suivantes :

    $ - cd dir/


    $ - git clone https://gitlab.com/jl-perez66/cv-symfony.git


    $ - composer install


Modifier par la suite le fichier *.env* afin de connecter le projet à votre base de données.

Puis, créer la base de donnée pour le projet Symfony : 

    $ - php bin/console doctrine:database:create

Appliquer par la suite les migrations */!\Supprimer les migrations existantes avant d'écrire ces lignes de commandes !*

    $ - php bin/console make:migration

    $ - php bin/console doctrine:migrations:migrate

Appliquer les fixtures présents sur le CV afin de remplir les données : 

    $ - php bin/console doctrine:fixtures:load
    
Pour finir, lancer le serveur 

    $ - symfony server:start

------

Merci ! 
    

